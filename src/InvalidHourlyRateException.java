/**
 * Created by Dmitry on 7/13/2015.
 */
public class InvalidHourlyRateException extends Exception {
    public InvalidHourlyRateException(double rate) {
        super("Invalid hourly pay rate: " + rate);
    }
}
